/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisfarm.classes;

/**
 *
 * @author Jeferson
 */
public class CadastroAcesso {
    
    private int idAcesso;
    private String nome;
    private String email;
    private String cadLogin;
    private String cadSenha;

    /**
     * @return the idAcesso
     */
    public int getIdAcesso() {
        return idAcesso;
    }

    /**
     * @param idAcesso the idAcesso to set
     */
    public void setIdAcesso(int idAcesso) {
        this.idAcesso = idAcesso;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the cadLogin
     */
    public String getCadLogin() {
        return cadLogin;
    }

    /**
     * @param cadLogin the cadLogin to set
     */
    public void setCadLogin(String cadLogin) {
        this.cadLogin = cadLogin;
    }

    /**
     * @return the cadSenha
     */
    public String getCadSenha() {
        return cadSenha;
    }

    /**
     * @param cadSenha the cadSenha to set
     */
    public void setCadSenha(String cadSenha) {
        this.cadSenha = cadSenha;
    }
    
}
