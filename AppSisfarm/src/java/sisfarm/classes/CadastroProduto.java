/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisfarm.classes;

/**
 *
 * @author Jeferson
 */
public class CadastroProduto {
    
    private int idProduto;
    private String nomeProduto;
    private String fornecedor;
    private String preco;
    private String promocao;
    private String urlImagem;

    /**
     * @return the idProduto
     */
    public int getIdProduto() {
        return idProduto;
    }

    /**
     * @param idProduto the idProduto to set
     */
    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    /**
     * @return the nomeProduto
     */
    public String getNomeProduto() {
        return nomeProduto;
    }

    /**
     * @param nomeProduto the nomeProduto to set
     */
    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    /**
     * @return the fornecedor
     */
    public String getFornecedor() {
        return fornecedor;
    }

    /**
     * @param fornecedor the fornecedor to set
     */
    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

    /**
     * @return the preco
     */
    public String getPreco() {
        return preco;
    }

    /**
     * @param preco the preco to set
     */
    public void setPreco(String preco) {
        this.preco = preco;
    }

    /**
     * @return the promocao
     */
    public String getPromocao() {
        return promocao;
    }

    /**
     * @param promocao the promocao to set
     */
    public void setPromocao(String promocao) {
        this.promocao = promocao;
    }

    /**
     * @return the urlImagem
     */
    public String getUrlImagem() {
        return urlImagem;
    }

    /**
     * @param urlImagem the urlImagem to set
     */
    public void setUrlImagem(String urlImagem) {
        this.urlImagem = urlImagem;
    }
    
}
